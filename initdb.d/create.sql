CREATE TABLE content
(
    id       SERIAL PRIMARY KEY,
    filename VARCHAR NOT NULL
);

CREATE TABLE content_access
(
    content_id INT       NOT NULL,
    client_ip  inet      NOT NULL,
    access_at  timestamp NOT NULL,
    CONSTRAINT fk_content
        FOREIGN KEY (content_id) REFERENCES content (id)
);
