<?php

namespace App\Controller;

use App\Service\ContentAccessInterface;
use App\Service\ContentRepositoryInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ContentController
 * @package App\Controller
 *
 * Контроллер для получения контента
 */
class ContentController
{
    /**
     * Получение контента
     *
     * @param $id
     * @param Request $request
     * @param ContentRepositoryInterface $contentRepository
     * @param ContentAccessInterface $contentAccess
     * @return Response
     */
    public function get(
        $id,
        Request $request,
        ContentRepositoryInterface $contentRepository,
        ContentAccessInterface $contentAccess
    ): Response {
        $clientIp = $request->getClientIp();
        if ($contentAccess->access($id, $clientIp)) {
            return $contentRepository->get($id) ?? new JsonResponse(['error' => 'Failed to stream file'], 500);
        } else {
            return new JsonResponse(['error' => 'Access denied'], 403);
        }
    }

    /**
     * Получение самых активных потребителей контента
     *
     * @param $id
     * @param Request $request
     * @param ContentAccessInterface $contentAccess
     * @return Response
     */
    public function stats(
        $id,
        Request $request,
        ContentAccessInterface $contentAccess
    ): Response {
        $limit = $request->get('limit', 5);
        $clients = $contentAccess->frequentClients($id, $limit);
        return new JsonResponse(['clients' => $clients]);
    }
}
