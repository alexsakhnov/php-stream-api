<?php

namespace App\Service;

/**
 * Class DBConnection
 * @package App\Service
 *
 * Подключение к базе данных
 */
class DBConnection
{
    /**
     * @var \PDO подключение к БД
     */
    private $pdo;

    public function __construct(string $dsn, string $user = null, string $password = null)
    {
        $this->pdo = new \PDO($dsn, $user, $password);
    }

    /**
     * Функция возвращает подключение к БД
     * @return \PDO
     */
    public function getConnection(): \PDO
    {
        return $this->pdo;
    }
}
