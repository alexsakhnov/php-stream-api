<?php

namespace App\Service;

/**
 * Interface ContentAccessInterface
 * @package App\Service
 *
 * Интерфейс доступа к контенту.
 */
interface ContentAccessInterface
{
    /**
     * Получение доступа к контенту $contentId.
     *
     * Функция возвращает true, если доступ разрешён. Иначе false.
     *
     * @param int $contentId
     * @param string $clientIp
     * @return bool
     */
    public function access(int $contentId, string $clientIp): bool;

    /**
     * Получение самых частых клиентов, загружавших контент $contentId
     *
     * Функция возвращает массив IP адресов самых частых клиентов в порядке убывания
     * количества запросов.
     *
     * @param int $contentId
     * @param int $limit
     * @return array
     */
    public function frequentClients(int $contentId, int $limit): array;
}
