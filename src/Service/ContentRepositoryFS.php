<?php

namespace App\Service;

use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Response;

class ContentRepositoryFS implements ContentRepositoryInterface
{
    /**
     * Каталог, в котором расположен контент
     * @var string
     */
    private $contentRoot;

    /**
     * Подключение к базе данных
     * @var DBConnection
     */
    private $db;

    public function __construct(DBConnection $db, string $contentRoot)
    {
        $this->db = $db;
        $this->contentRoot = $contentRoot;
    }

    /**
     * @inheritDoc
     */
    public function get(int $contentId): ?Response
    {
        $query = "SELECT filename FROM content WHERE id = :content_id LIMIT 1";

        $stmt = $this->db->getConnection()->prepare($query);
        $stmt->execute([':content_id' => $contentId]);
        $result = $stmt->fetchColumn(0);

        if ($result === false) {
            return null;
        }

        $filename = $this->contentRoot . DIRECTORY_SEPARATOR . ((string)$result);
        if (!is_file($filename)) {
            error_log("Content file not found: $filename");
            return null;
        }

        return new BinaryFileResponse($filename);
    }
}
