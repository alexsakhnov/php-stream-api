<?php

namespace App\Service;

use Symfony\Component\HttpFoundation\Response;

/**
 * Interface ContentRepositoryInterface
 * @package App\Service
 *
 * Интерфейс для получения контента по его идентификатору.
 */
interface ContentRepositoryInterface
{
    /**
     * Получает контент по его идентификатору.
     *
     * Возвращает HTTP-ответ, если файл найден; иначе null.
     *
     * @param int $contentId идентификатор контента
     * @return Response
     */
    public function get(int $contentId): ?Response;
}
