<?php

namespace App\Service;

/**
 * Class ContentAccessSimple
 * @package App\Service
 *
 * Класс доступа к контенту.
 */
class ContentAccessSimple implements ContentAccessInterface
{
    /**
     * @var int максимальное число запросов на получение контента за указанных интервал
     */
    private $limit;

    /**
     * @var int интервал, в секундах
     */
    private $limitInterval;

    /**
     * @var DBConnection подключение к базе данных
     */
    private $db;

    /**
     * ContentAccessSimple constructor.
     * @param DBConnection $db подключение к базе данных
     * @param $limitRequests int максимальное число запросов на получение контента за указанных интервал
     * @param $limitInterval int интервал, в секундах
     */
    public function __construct(DBConnection $db, int $limitRequests, int $limitInterval)
    {
        $this->db = $db;
        $this->limit = $limitRequests;
        $this->limitInterval = $limitInterval;
    }

    /**
     * @inheritDoc
     */
    public function access(int $contentId, string $clientIp): bool
    {
        // Фиксируем доступ к контенту, если соблюден rate limit.
        $query = <<<EOT
        with rows as (
            INSERT INTO content_access (content_id, client_ip, access_at)
                SELECT :content_id,
                       (:client_ip)::inet,
                       now()
                WHERE :limit_requests > (
                    SELECT count(*)
                    FROM content_access
                    WHERE client_ip = (:client_ip)::inet
                      AND access_at > now() - (:limit_interval || ' second')::INTERVAL
                )
                RETURNING 1
        )
        SELECT count(*) FROM rows;
        EOT;

        $stmt = $this->db->getConnection()->prepare($query);

        $stmt->execute(
            [
                ':content_id' => $contentId,
                ':client_ip' => $clientIp,
                ':limit_requests' => $this->limit,
                ':limit_interval' => $this->limitInterval,
            ]
        );

        return (int)$stmt->fetchColumn(0) === 1;
    }

    public function frequentClients(int $contentId, int $limit): array
    {
        $query = <<<EOT
        SELECT client_ip
        FROM content_access
        WHERE content_id = :content_id
        GROUP BY client_ip
        ORDER BY count(*) DESC
        LIMIT :limit
        EOT;

        $stmt = $this->db->getConnection()->prepare($query);
        $stmt->execute(
            [
                ':content_id' => $contentId,
                ':limit' => $limit,
            ]
        );

        return $stmt->fetchAll(\PDO::FETCH_COLUMN, 0);
    }
}
