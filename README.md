# API для получения контента

## Установка и запуск

```
docker-compose run php composer install
docker-compose up
```

Параметры подключения к БД указаны в файле [.env](./.env)

Файлы для загрузки хранятся в директории `./content`

## API

- `GET /content/{id}` - получение контента {id}
- `GET /content/{id}/stats?limit={limit}` - возвращает `{limit}` IP адресов, которые скачали
 контент `{id}` больше всего раз (в порядке убывания количества скачиваний).

